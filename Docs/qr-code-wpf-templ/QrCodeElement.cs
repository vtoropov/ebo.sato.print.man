using System.Collections.Generic;
using System.ComponentModel;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using Gma.QrCodeNet.Encoding;

namespace Gma.QrCodeNet.Wpf
{
    public class QrCodeElement : Control
    {
        public static readonly DependencyProperty TextProperty = DependencyProperty.Register("Text", typeof (string), typeof (QrCodeElement), new UIPropertyMetadata("", (o, args) => ((QrCodeElement) o).UpdatePath()));
        public static readonly DependencyProperty ErrorCorrectionProperty = DependencyProperty.Register("ErrorCorrection", typeof (ErrorCorrectionLevel), typeof (QrCodeElement), new UIPropertyMetadata(ErrorCorrectionLevel.M, (o, args) => ((QrCodeElement) o).UpdatePath()));
        public static readonly DependencyPropertyKey PathGeometryProperty = DependencyProperty.RegisterReadOnly("PathGeometry", typeof (Geometry), typeof (QrCodeElement), new UIPropertyMetadata(new RectangleGeometry(new Rect(0, 0, 1, 1))));

        static QrCodeElement()
        {
            DefaultStyleKeyProperty.OverrideMetadata(typeof (QrCodeElement), new FrameworkPropertyMetadata(typeof (QrCodeElement)));
            BackgroundProperty.OverrideMetadata(typeof (QrCodeElement), new FrameworkPropertyMetadata(Brushes.White));
            ForegroundProperty.OverrideMetadata(typeof (QrCodeElement), new FrameworkPropertyMetadata(Brushes.Black));
            PaddingProperty.OverrideMetadata(typeof (QrCodeElement), new FrameworkPropertyMetadata(new Thickness(20)));
        }

        public QrCodeElement()
        {
            UpdatePath();
        }

        public Geometry PathGeometry
        {
            get { return (Geometry) GetValue(PathGeometryProperty.DependencyProperty); }
            private set { SetValue(PathGeometryProperty, value); }
        }

        [Category("Common Properties")]
        public string Text
        {
            get { return (string) GetValue(TextProperty); }
            set { SetValue(TextProperty, value); }
        }

        [Category("Common Properties")]
        public ErrorCorrectionLevel ErrorCorrection
        {
            get { return (ErrorCorrectionLevel) GetValue(ErrorCorrectionProperty); }
            set { SetValue(ErrorCorrectionProperty, value); }
        }

        void UpdatePath()
        {
            PathGeometry = new GeometryGroup {Children = new GeometryCollection(GetFilledSquares())};
        }

        IEnumerable<Geometry> GetFilledSquares()
        {
            BitMatrix bitMatrix = new QrEncoder(ErrorCorrection).Encode(Text).Matrix;

            for (int x = 0; x < bitMatrix.Width; x++)
            {
                for (int y = 0; y < bitMatrix.Height; y++)
                {
                    if (bitMatrix[x, y])
                    {
                        yield return new RectangleGeometry(new Rect(x, y, 1, 1));
                    }
                }
            }
        }
    }
}