﻿using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media.Imaging;
using Microsoft.Win32;

using FirstFloor.ModernUI.Windows.Controls;

namespace Sato.Core.UI.Content {

    public partial class PictureSend : UserControl {

		private PictureView m_view = null;

		public bool IsDefault { get; set; }

		public PictureSend() {
			m_view = new PictureView();

			InitializeComponent();

			this.IsDefault = true;

			this.Loaded += OnLoaded;
			this.DataContext = m_view;
		}

		bool IsConnectSelected() {
			int n_selected = this.SendList.SelectedIndex;
			if (n_selected < 0) {
				ModernDialog.ShowMessage(
						"Remote printer connection is not selected;", "Error", MessageBoxButton.OK
					); return false;
			}
			else
				return true;
		}

		void OnBrowse (object sender, RoutedEventArgs e) {
			bool b_result  = m_view.Browse();
			if ( b_result == true) {
				PictureImage.Source = new BitmapImage(new Uri(m_view.Path));
			}
			this.IsDefault = !b_result;
			this.Default.IsEnabled = b_result;
		}

		void OnDefault(object sender, RoutedEventArgs e) {
			// image file must have the property [Build Action = Resource]; otherwise it wont be laoded;
			PictureImage.Source = new BitmapImage(new Uri("/Resources/typeset.ico.new.40.png", UriKind.Relative));
			this.Default.IsEnabled = false;
		}

		void OnSendToPrint(object sender, RoutedEventArgs e) {
			bool b_result  = m_view.IsReady();
			if ( b_result == false ) {
				ModernDialog.ShowMessage( m_view.Error +
						" Default image cannot be sent to printer in this version;", "Warning", MessageBoxButton.OK
					);  return;
			}
			if (this.IsConnectSelected() == false) {
				return;
			}

			this.Cursor = Cursors.Wait ;
			b_result    = m_view.SendToPrint(this.SendList.SelectedIndex);
			this.Cursor = Cursors.Arrow;

			if (false == b_result) {
				ModernDialog.ShowMessage(
					m_view.Error + ";", "Error", MessageBoxButton.OK
				);
			}
			else
				ModernDialog.ShowMessage(
					"Sending picture to printer is completed;", "Info", MessageBoxButton.OK
				);
		}

		void OnConnListRefresh(object sender, RoutedEventArgs e) {

			Helpers.ConnectFeeder feeder = new Helpers.ConnectFeeder();
			this.SendCmd.IsEnabled = feeder.SeedList(ref this.SendList);
		}

		void OnLoaded(object sender, RoutedEventArgs e) {
			if (this.IsDefault)
				this.OnDefault(sender, e);
			
			Keyboard.Focus(this.Browse);
			this.SendCmd.IsEnabled   = false;
			this.Prog_bar.Visibility = Visibility.Hidden;
		}
	}
}
