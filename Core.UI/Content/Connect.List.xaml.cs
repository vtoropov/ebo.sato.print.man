﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

using FirstFloor.ModernUI.Windows.Controls;

namespace Sato.Core.UI.Content
{
    // taken from MSDN (http://msdn.microsoft.com/en-us/library/system.windows.controls.datagrid.aspx)

    public partial class ConnectList : UserControl
    {
        public ConnectList()  {
			InitializeComponent();

			this.Loaded     += OnLoaded ;
			this.DataContext = GetData();
		}

        private ObservableCollection<Sato.Core.Data.Connect> GetData() {
            var connects = new ObservableCollection<Sato.Core.Data.Connect>();

			Sato.Core.Data.Connect_Enum conn_enum = new Sato.Core.Data.Connect_Enum();
			conn_enum.Load();
			List<Sato.Core.Data.Connect> lst_ = conn_enum.List();

			foreach (Sato.Core.Data.Connect cust_ in lst_) {
				connects.Add(cust_);
			}

			return connects;
        }

		private System.Windows.Controls.DataGridCell GetDataGridCell(System.Windows.Controls.DataGridCellInfo cellInfo) {
			var cellContent = cellInfo.Column.GetCellContent(cellInfo.Item);
			if (cellContent != null)
				return ((System.Windows.Controls.DataGridCell)cellContent.Parent);
			return (null);
		}

		/////////////////////////////////////////////////////////////////////////////

		private void OnDelete(object sender, RoutedEventArgs e) {

			int currentRowIndex = this.Conn_Table.SelectedIndex;
			if (currentRowIndex < 0) {
				ModernDialog.ShowMessage("Connection is not selected;", "Warning", MessageBoxButton.OK);
				return;
			}

			var cellInfo = this.Conn_Table.SelectedCells[0];
			var content  = (cellInfo.Column.GetCellContent(cellInfo.Item) as TextBlock).Text;

			MessageBoxResult e_result = ModernDialog.ShowMessage(
				"Do you want to delete selected connection:\n" + content + "?", "Deleting Connection", MessageBoxButton.YesNo);

			if (e_result == MessageBoxResult.No)
				return;

			Sato.Core.Data.Connect_Enum conn_enum = new Sato.Core.Data.Connect_Enum();

			if (conn_enum.Remove(content))
				Conn_Table.DataContext = GetData();
		}

		private void OnLoaded(object sender, RoutedEventArgs e) {
#if __use_it
			Keyboard.Focus(GetDataGridCell(this.Conn_Table.SelectedCells[0]));
#else
			Keyboard.Focus(this.Conn_Table);
			this.EditItm.IsEnabled = false;
			this.AddNew.IsEnabled  = false;
#endif
		}

		private void OnRefresh(object sender, RoutedEventArgs e) {
			Conn_Table.DataContext = GetData();
		}
	}
}
