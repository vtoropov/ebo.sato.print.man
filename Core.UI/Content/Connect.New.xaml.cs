﻿using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

using FirstFloor.ModernUI.Windows.Controls;

namespace Sato.Core.UI.Content {

	public partial class ConnectForm : UserControl {

		private ConnectView view_ = null;

		public ConnectForm() {
			view_ = new ConnectView();

			InitializeComponent();

			this.Loaded += OnLoaded ;
            this.DataContext = view_;
        }

		void OnConnectSave(object sender, RoutedEventArgs e) {
			bool b_result = view_.Save();
			if ( b_result == false )
				ModernDialog.ShowMessage(view_.Error, "Error", MessageBoxButton.OK);
			else {
				view_ = new ConnectView();
				this.DataContext = view_;
			}
		}

		void OnLoaded(object sender, RoutedEventArgs e) {
            Keyboard.Focus(this.TextName);
        }
	}
}
