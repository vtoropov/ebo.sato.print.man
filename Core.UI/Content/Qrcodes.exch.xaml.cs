﻿using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;

using FirstFloor.ModernUI.Windows.Controls;

namespace Sato.Core.UI.Content {

    public partial class QrCodesExch : UserControl {

		private QrCodeView view_ = null;

		public QrCodesExch() {
			view_ = new QrCodeView();

			InitializeComponent();

			this.Loaded += OnLoaded;
			this.DataContext = view_;
		}

		void OnConvert(object sender, RoutedEventArgs e) {
			bool b_result  = view_.Convert();
			if ( b_result == false)
				ModernDialog.ShowMessage(view_.Error, "Error", MessageBoxButton.OK);
			else {
				RenderOptions.SetBitmapScalingMode(QrCodeImage, BitmapScalingMode.NearestNeighbor);
				QrCodeImage.Source = view_.Data;
			}
		}

		void OnCodeSend(object sender, RoutedEventArgs e) {
			bool b_result  = false;
			if ( b_result == false)
				ModernDialog.ShowMessage("This functionality is not implemented yet;", "Info", MessageBoxButton.OK);
			else {
			}
		}

		void OnConnListRefresh(object sender, RoutedEventArgs e) {
			bool b_result  = false;

			Sato.Core.Data.Connect_Enum conn_enum = new Sato.Core.Data.Connect_Enum();
			conn_enum.Load();
			List<Sato.Core.Data.Connect> lst_ = conn_enum.List();

			this.SendList.Items.Clear();

			foreach (Sato.Core.Data.Connect cust_ in lst_) {
				this.SendList.Items.Add(cust_.Tcp + ":" + cust_.Port);
			}
			b_result = lst_.Count > 0;

			if (b_result == false) {
				this.SendList.Items.Add("Press [Refresh] button;");
				this.SendList.SelectedIndex = 0;
				this.SendCmd.IsEnabled = false;
			}
			else {
				this.SendList.SelectedIndex = 0;
				this.SendCmd.IsEnabled = true;
			}
		}

		void OnLoaded(object sender, RoutedEventArgs e) {
			Keyboard.Focus(this.Input);
			this.SendCmd.IsEnabled = false;
		}
	}
}
