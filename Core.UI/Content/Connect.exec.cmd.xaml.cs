﻿using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;

using System.ComponentModel;
using System.Threading;

namespace Sato.Core.UI.Content {

	public partial class ConnectExec : UserControl
	{
		private BackgroundWorker bak_worker;
		private List<Sato.Core.Data.Connect> m_conns = null;

		public ConnectExec() {
			bak_worker = new BackgroundWorker {
				WorkerReportsProgress      = true,
				WorkerSupportsCancellation = true,
			};
			bak_worker.DoWork += new DoWorkEventHandler(DoWork);
			bak_worker.RunWorkerCompleted += new RunWorkerCompletedEventHandler(Completed);
			bak_worker.ProgressChanged += new ProgressChangedEventHandler(ProgressChanged);

			InitializeComponent();
			this.Loaded += OnLoaded;
		}

		void OnListRefresh(object sender, RoutedEventArgs e) {

			Helpers.ConnectFeeder feeder = new Helpers.ConnectFeeder();
			this.ExecuteBut.IsEnabled = feeder.SeedList(ref this.SendList);

			this.ExecStatus.Content = "Awaiting command execution.";
			this.ExecStatus.Foreground = new SolidColorBrush(Colors.Blue);
		}

		void OnLoaded (object sender, RoutedEventArgs e) {
			Keyboard.Focus(this.ReqText);
			this.ExecuteBut.IsEnabled  = false;
			this.Progress.Visibility   = Visibility.Hidden;
		}

		void OnExecute(object sender, RoutedEventArgs e){

			SolidColorBrush err_brush = new SolidColorBrush(Colors.Red);

			Helpers.ConnectFeeder feeder = new Helpers.ConnectFeeder();
			this.m_conns = feeder.Connections();

			if (this.m_conns == null) {
				this.ExecStatus.Content = "Error: Connection list is not initialized;";
				this.ExecStatus.Foreground = err_brush;
				return;
			}

			int n_selected = this.SendList.SelectedIndex;
			if (n_selected < 0 || n_selected >= m_conns.Count) {
				this.ExecStatus.Content = "Error: Connection is not selected;";
				this.ExecStatus.Foreground = err_brush;
				return;
			}
			// TODO: it very looks like data view must be created, no multiple base classes are acceptable;
			this.Request = this.ReqText.Text;

			if (string.IsNullOrEmpty(this.Request)) {
				this.ExecStatus.Content = "Error: Command text for sending is empty;";
				this.ExecStatus.Foreground = err_brush;
				return;
			}

			Sato.Core.Data.Connect selected_conn = m_conns[n_selected];
			if (selected_conn.Is() == false) {
				this.ExecStatus.Content = "Error: " + selected_conn.Error + ";";
				this.ExecStatus.Foreground = new SolidColorBrush(Colors.Red);
			}
			else {
				this.Cursor = Cursors.Wait;
				try {
					this.Wait(true);

					Sato.Core.Printer.PrinterIface prn_iface = new Sato.Core.Printer.PrinterIface();

					if (prn_iface.Send(this.Request, ref selected_conn) == false) {
						this.ExecStatus.Content = "Error: " + prn_iface.Error + ";";
						this.ExecStatus.Foreground = err_brush;
					}
					else {
						this.ExecStatus.Content = "Sending command text is completed;";
						this.ExecStatus.Foreground = new SolidColorBrush(Colors.Green);
					}
				}
				finally {
					this.Wait(false);
				}
			}
		}

		public string Request { get; set; }
		private void  Wait (bool _state) {
			if (_state) {
				this.Cursor = Cursors.Wait;
				this.Prog_bar.Visibility   = Visibility.Visible;
				this.Prog_bar.UpdateLayout();
				this.ExecuteBut.IsEnabled  = false;
				this.ExecStatus.Content    = "Sending command text to printer...";
				this.ExecStatus.Foreground = new SolidColorBrush(Colors.Blue);
			}
			else {
				this.Cursor = Cursors.Arrow;
				this.ExecuteBut.IsEnabled  = true;
				this.Prog_bar.Visibility   = Visibility.Hidden;
			}
		}

/////////////////////////////////////////////////////////////////////////////

		private void DoWork(object sender, DoWorkEventArgs e) {
		}

		private void Completed(object sender, RunWorkerCompletedEventArgs e) {
		}

		private void ProgressChanged(object sender, ProgressChangedEventArgs e) {
		}

	}
}
