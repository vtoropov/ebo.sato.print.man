﻿using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;

using FirstFloor.ModernUI.Windows.Controls;

namespace Sato.Core.UI.Content {

	public partial class ConnectWait : UserControl {

		private List<Sato.Core.Data.Connect> m_conns = null;

		public ConnectWait() {

			InitializeComponent();

			this.Loaded += OnLoaded;
		}

		void OnListRefresh(object sender, RoutedEventArgs e) {

			Helpers.ConnectFeeder feeder = new Helpers.ConnectFeeder();
			this.TestButt.IsEnabled = feeder.SeedList(ref this.SendList);

			this.TestStatus.Content = "Awaiting connection test start.";
			this.TestStatus.Foreground = new SolidColorBrush(Colors.Blue);
		}

		void OnLoaded (object sender, RoutedEventArgs e) {
			Keyboard.Focus(this.RefreshBut);
			this.TestButt.IsEnabled  = false;
			this.Progress.Visibility = Visibility.Hidden;
		}

		void OnTestRun(object sender, RoutedEventArgs e){

			this.Progress.Visibility = Visibility.Hidden;

			int n_selected = this.SendList.SelectedIndex;
			if (n_selected < 0) {
				this.TestStatus.Content = "Error: Connection is not selected;";
				this.TestStatus.Foreground = new SolidColorBrush(Colors.Red);
				return;
			}

			Helpers.ConnectFeeder feeder = new Helpers.ConnectFeeder();
			this.m_conns = feeder.Connections();

			if (this.m_conns == null) {
				this.TestStatus.Content = "Error: Connection list is not initialized;";
				this.TestStatus.Foreground = new SolidColorBrush(Colors.Red);
				return;
			}
			Sato.Core.Data.Connect selected_conn = m_conns[n_selected];
			if (selected_conn.Is() == false) {
				this.TestStatus.Content = "Error: " + selected_conn.Error + ";";
				this.TestStatus.Foreground = new SolidColorBrush(Colors.Red);
			}
			else {
				this.Cursor = Cursors.Wait;
				try {
					this.Prog_bar.Visibility = Visibility.Visible;
					this.TestButt.IsEnabled  = false;
					this.TestStatus.Content  = "Sending test data to printer...";
					this.TestStatus.Foreground = new SolidColorBrush(Colors.Blue);

					Sato.Core.Printer.PrinterIface prn_iface = new Sato.Core.Printer.PrinterIface();

					if (prn_iface.Send("Test", ref selected_conn) == false) {
						this.TestStatus.Content = "Error: " + prn_iface.Error + ";";
						this.TestStatus.Foreground = new SolidColorBrush(Colors.Red);
					}
					else {
						this.TestStatus.Content = "Testing connection is success;";
						this.TestStatus.Foreground = new SolidColorBrush(Colors.Green);
					}
				}
				finally {
					this.Cursor = Cursors.Arrow;
					this.TestButt.IsEnabled = true;
					this.Prog_bar.Visibility = Visibility.Hidden;
				}
			}
		}

	}
}
