﻿using System.Windows.Media;
using System.Windows.Media.Imaging;

using System.ComponentModel;
using FirstFloor.ModernUI.Presentation;

using Gma.QrCodeNet.Encoding;
using Gma.QrCodeNet.Encoding.Windows.Render;

namespace Sato.Core.UI.Content
{
	public class QrCodeView : NotifyPropertyChanged, IDataErrorInfo {

		private string m_error = "";

		public string Error { get { return this.m_error; } }
		public string this  [string _property]  {
			get {
				if (_property == "Input_do_not_use_it_because_button_is_covered_by_tip") {
					return string.IsNullOrEmpty(this.Input) ? "Required value" : null;
				}
				return null;
			}
		}

		private Sato.Core.Data.QrParser parser_ = new Sato.Core.Data.QrParser();

		private string m_input   = "";
		private string m_encoded = "";
		private WriteableBitmap m_qrcode = new WriteableBitmap(70, 70, 96, 96, PixelFormats.Gray8, null);

		public WriteableBitmap Data {
			get { return this.m_qrcode; }
		}

		public string Encoded {
			get { return this.m_encoded;  }
			set {
				if (this.m_encoded != value) {
					this.m_encoded  = value;
					OnPropertyChanged("Encoded");
				}
			}
		}

		public string Input {
			get { return this.m_input; }
			set {
				if (this.m_input != value) {
					this.m_input  = value;
					OnPropertyChanged("Input");
				}
			}
		}

		public void Clear() {
			parser_ = new Sato.Core.Data.QrParser();
		}

		public bool Convert() {
			m_error = "";
			bool b_result = !string.IsNullOrEmpty(this.Input);
			if ( b_result == false) {
				m_error = "Input string is empty or null;";
				return b_result;
			}
			this.Encoded = parser_.Encoded(this.Input);
			b_result  = !string.IsNullOrEmpty(this.Encoded);
			if (b_result == false)
				m_error = parser_.Error;
			else {
				Sato.Core.Render.QrBitmap bmp_ = new Sato.Core.Render.QrBitmap(); bmp_.Scale = 3;
				b_result = bmp_.Generate(parser_.Matrix);
				if (b_result) {
					m_qrcode = bmp_.Bitmap;
				}
			}
			return b_result;
		}
	}
}
