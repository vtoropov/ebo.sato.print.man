using System.ComponentModel;
using FirstFloor.ModernUI.Presentation;

namespace Sato.Core.UI.Content
{
    /// <summary>
    /// </summary>
    public class ConnectView
        : NotifyPropertyChanged, IDataErrorInfo
    {
		public string Error { get; private set; }
		public  string this[string _property] {
			get {
				if (_property == "Name") {
					return string.IsNullOrEmpty(this.conn_.Name) ? "Required value" : null;
				}
				if (_property == "Tcp") {
					return string.IsNullOrEmpty(this.conn_.Tcp ) ? "Required value" : null;
				}
				if (_property == "Port") {
					return string.IsNullOrEmpty(this.conn_.Tcp ) ? "Required value" : null;
				}
				if (_property == "Timeout") {
					return string.IsNullOrEmpty(this.conn_.Tcp ) ? "Required value" : null;
				}
				return null;
			}
		}

		private Sato.Core.Data.Connect conn_ = new Sato.Core.Data.Connect();

		public void Clear() {
			conn_ = new Sato.Core.Data.Connect();
		}

		public string Name {
			get { return this.conn_.Name; }
			set {
				if (this.conn_.Name != value) {
					this.conn_.Name  = value;
					OnPropertyChanged("Name");
				}
			}
		}
		public string Note {
			get { return this.conn_.Note; }
			set {
				if (this.conn_.Note != value) {
					this.conn_.Note  = value;
					OnPropertyChanged("Note");
				}
			}
		}
		public string Tcp {
			get { return this.conn_.Tcp;  }
			set {
				if (this.conn_.Tcp != value) {
					this.conn_.Tcp  = value;
					OnPropertyChanged("Tcp");
				}
			}
		}
		public string Port {
			get { return this.conn_.Port.ToString();  }
			set {
				uint u_port = 0;
				if (uint.TryParse(value, out u_port) == false) {
					this.Error = "Port value must be numeric;";
				}
				if (this.conn_.Port != u_port) {
					this.conn_.Port  = u_port;
					OnPropertyChanged("Port");
				}
			}
		}
		public string Timeout {
			get { return this.conn_.Timeout.ToString(); }
			set {
				uint u_timeout = 0;
				if (uint.TryParse(value, out u_timeout) == false) {
					this.Error = "Timeout value must be numeric;";
				}
				if (this.conn_.Timeout != u_timeout) {
					this.conn_.Timeout = u_timeout;
					OnPropertyChanged("Timeout");
				}
			}
		}

		public bool Save () {
			return this.conn_.Save();
		}
	}
}
