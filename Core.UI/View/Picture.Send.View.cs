﻿using System;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.ComponentModel;

using Microsoft.Win32;

using FirstFloor.ModernUI.Presentation;

namespace Sato.Core.UI.Content
{
	public class PictureView : NotifyPropertyChanged {

		private string m_error = "";
		private string m_path  = "";

		public string Error { get { return this.m_error; } }
		public string Path { get { return this.m_path; } }

		public bool Browse() {
			OpenFileDialog dlg_ = new OpenFileDialog();
			dlg_.Title = "Select a picture";
			dlg_.Filter = "Pictures|*.jpg;*.jpeg;*.png|JPEG (*.jpg;*.jpeg)|*.jpg;*.jpeg|Portable Network Graphic (*.png)|*.png";
			bool b_result = (dlg_.ShowDialog() == true);
			if (b_result) {
				this.m_path = dlg_.FileName;
			}
			else
				this.m_path = "";
			return b_result;
		}

		public bool IsReady() {
			if (string.IsNullOrEmpty(this.m_path)) {
				m_error = "Source image file is not selected;"; return false;
			}
			else {
				m_error = ""; return true;
			}
		}

		public PictureView() {
		}

		public bool SendToPrint(int n_selected) {

			bool b_result = false;
			Sato.Core.Data.Connect_Enum conn_enum = new Sato.Core.Data.Connect_Enum();
			conn_enum.Load();
			Sato.Core.Data.Connect selected_conn = conn_enum.Get(n_selected);
			if (selected_conn.Is() == false) {
				this.m_error = selected_conn.Error ;
				return b_result;
			}

			Sato.Core.Render.CPictureWrapper wrap_ = new Sato.Core.Render.CPictureWrapper();

			b_result = wrap_.Convert(m_path);
			if (b_result == false) {
				m_error = wrap_.Error; return b_result;
			}

			Sato.Core.Printer.PrinterIface prn_iface = new Sato.Core.Printer.PrinterIface();

			b_result = prn_iface.Send(wrap_.Command, ref selected_conn);
			if (b_result == false) {
				m_error = prn_iface.Error; return b_result;
			}
			return b_result;
		}
	}
}
