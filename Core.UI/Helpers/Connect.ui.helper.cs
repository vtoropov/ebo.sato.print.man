﻿using System.Collections.Generic;
using System.Windows.Controls;

namespace Sato.Core.UI.Helpers {

	class ConnectFeeder {

		public ConnectFeeder() {}

		public List<Sato.Core.Data.Connect> Connections() {
			Sato.Core.Data.Connect_Enum conn_enum = new Sato.Core.Data.Connect_Enum();
			conn_enum.Load();
			return conn_enum.List();
		}

		public bool SeedList (ref ComboBox _list) {
			bool b_result = false;
			_list.Items.Clear();

			Sato.Core.Data.Connect_Enum conn_enum = new Sato.Core.Data.Connect_Enum();
			conn_enum.Load();

			List<Sato.Core.Data.Connect> conn_lst = conn_enum.List();
			foreach (Sato.Core.Data.Connect connect in conn_lst) {
				_list.Items.Add(connect.Name + " : " + connect.Tcp + ":" + connect.Port);
			}

			b_result = conn_lst.Count > 0;
			if (conn_lst.Count == 0) {
				_list.Items.Add("Press [Refresh] button;");
			}

			_list.SelectedIndex = 0;

			return b_result;
		}
	}
}
