﻿using System.ComponentModel;
using System.Threading;

using SATOPrinterAPI;
using Sato.Core.Data;

namespace Sato.Core.Printer {

	class PrinterReceiverAsync {

		private BackgroundWorker bak_worker;

		public string Error    { get; private set; }
		public string Received { get; private set; }
		public int    Progress { get; private set; }

	public PrinterReceiverAsync() {

			bak_worker = new BackgroundWorker();
			bak_worker.WorkerReportsProgress      = true;
			bak_worker.WorkerSupportsCancellation = true;

			bak_worker.DoWork += new DoWorkEventHandler(DoWork);
			bak_worker.RunWorkerCompleted += new RunWorkerCompletedEventHandler(Completed);
			bak_worker.ProgressChanged += new ProgressChangedEventHandler(ProgressChanged);
		}

		// synchronous call for canceling background worker;
		public bool Cancel () {
			bool   b_result = false;
			if (bak_worker == null) {
				this.Error = "Worker object is not created;";
				return b_result;
			} else if (bak_worker.IsBusy == false) {
				this.Error = "Worker object is not started;";
				return b_result;
			}
			else {
				bak_worker.CancelAsync();
			}
			return (b_result = true);
		}

		// synchronous call for starting background worker; for cases printer permanent connection;
		public void Start (object sender, SATOPrinterAPI.Printer.ByteAvailableEventArgs e) {

			if (bak_worker == null) {
				this.Error = "Worker object is not created;";
			} else if (bak_worker.IsBusy) {
				this.Error = "Worker object is busy;";
			}
			else {
				this.Progress = 0;
				this.Received = "";
				this.Error    = "";
				bak_worker.RunWorkerAsync(argument: e.Data);
			}
		}
		// https://docs.microsoft.com/en-us/dotnet/framework/winforms/controls/how-to-run-an-operation-in-the-background
		private void DoWork(object sender, DoWorkEventArgs e) {
			BackgroundWorker bw = sender as BackgroundWorker;
			PrinterData prn_dat = new PrinterData();
			while (bw.CancellationPending == false) {
				byte[] raw_data = (byte[])e.Argument;
				if (raw_data != null && raw_data.Length > 0)
					e.Result  = prn_dat.ControlCharConvert(Utils.ByteArrayToString(raw_data));
				Thread.Sleep(100);
			}
			e.Cancel = true;
		}

		private void Completed(object sender, RunWorkerCompletedEventArgs e) {
			if (e.Error != null) {
				this.Error = e.Error.Message;
			}
			else if (e.Cancelled) {
				this.Received = "";
				this.Error = "Data receiving is canceled;";
			}
			else {
				this.Error = "";
				this.Received += (string)e.Result;
			}
		}

		private void ProgressChanged(object sender,
			ProgressChangedEventArgs e) {
			this.Progress = e.ProgressPercentage;
		}

	}
}
