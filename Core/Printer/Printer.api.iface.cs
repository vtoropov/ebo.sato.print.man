﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using SATOPrinterAPI;
using Sato.Core.Data;

namespace Sato.Core.Printer {

	public class PrinterIface {

		private PrinterReceiverAsync   m_receiver = null;
		private SATOPrinterAPI.Printer m_printer  = null;

		public Connect Current  { get; private set; }
		public string Error     { get; private set; }
		public bool IsConnected { get; private set; }
		public bool IsPermanent {
			
			get {
				return m_printer == null ? false : m_printer.PermanentConnect;
			}
			set {
				if (m_printer == null)
					this.Error = "Printer object is not initialized;";
				if (m_receiver == null)
					this.Error = "Printer data receiver is not created;";
				else if (m_printer.PermanentConnect != value) {
					this.Error = "";

					try {
						if (value == true) {
							if (this.Current.Is() == false) {
								this.Error = this.Current.Error; return;
							}
							m_printer.PermanentConnect = value;
							m_printer.ByteAvailable   += m_receiver.Start;
							m_printer.TCPIPAddress   = this.Current.Tcp;
							m_printer.TCPIPPort      = this.Current.Port.ToString();
							m_printer.Timeout        = (int)this.Current.Timeout;

							m_printer.Connect();
							this.IsConnected = true;
						}
						else {
							m_printer.ByteAvailable -= m_receiver.Start; m_receiver.Cancel();
							m_printer.PermanentConnect = value;

							m_printer.Disconnect();
							this.IsConnected = false;
						}

					} catch (Exception ex_) { this.Error = ex_.Message; }
				}
			}
		}

		public PrinterIface() {
#if __leads_2_bug__Printer_data_receiver_is_not_created
			this.IsConnected = false;
			this.IsPermanent = false;
#endif
			try {
				m_receiver = new PrinterReceiverAsync();
				m_printer  = new SATOPrinterAPI.Printer();
				m_printer.Interface = SATOPrinterAPI.Printer.InterfaceType.TCPIP;
			}
			catch (Exception ex_) {
				this.Error = ex_.Message;
			}
			finally {
				this.IsConnected = false;
				this.IsPermanent = false;
			}
		}

		public bool  Query (string _data, ref Connect _conn, out string _received) {
			bool   b_result = false; _received = "";

			if (string.IsNullOrEmpty(_data)) {
				this.Error = "Send string data is null or empty;";
				return b_result;
			}
			if (_conn.Is() == false) {
				this.Error = _conn.Error;
				return b_result;
			}
			else
				this.Current = _conn; // updates current connect data;

			try {
				PrinterData prn_buffer = new PrinterData();
				byte[] cmddata = Utils.StringToByteArray(prn_buffer.ControlCharReplace(_data));

				m_printer.TCPIPAddress = this.Current.Tcp;
				m_printer.TCPIPPort    = this.Current.Port.ToString();
				m_printer.Timeout      = (int)this.Current.Timeout;


				byte[]  raw_data = m_printer.Query(cmddata);
				if (raw_data.Length > 0) {
					_received = Utils.ByteArrayToString(raw_data);
				}
			}
			catch (Exception ex_) {
				this.Error = ex_.Message;
			}
			return b_result;
		}

		public bool  Send (string _data, ref Connect _conn) {
			bool   b_result = false;
			if (string.IsNullOrEmpty(_data)) {
				this.Error = "Send string data is null or empty;";
				return b_result;
			}
			if (_conn.Is() == false) {
				this.Error = _conn.Error;
				return b_result;
			}
			else
				this.Current = _conn; // updates current connect data;

			try {
				PrinterData prn_buffer = new PrinterData();
				byte[] cmddata = Utils.StringToByteArray(prn_buffer.ControlCharReplace(_data));

				m_printer.TCPIPAddress = this.Current.Tcp;
				m_printer.TCPIPPort    = this.Current.Port.ToString();
				m_printer.Timeout      = (int)this.Current.Timeout;


				m_printer.Send(cmddata);
			}
			catch (Exception ex_) {
				this.Error = ex_.Message;
			}
			return b_result;
		}

		public string Version { get { return "3.0.0.1"; }}
	}
}
