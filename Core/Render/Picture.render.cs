﻿using System.Windows;
using System.Windows.Media;
using System.Windows.Media.Imaging;

using SATOPrinterAPI;

namespace Sato.Core.Render {

	public class CPictureWrapper {

		private string m_error   = "";
		private string m_command = "";

		public CPictureWrapper() { }

		public string Command { get { return this.m_command; } }
		public string Error   { get { return this.m_error; } }

		public bool Convert( string _file_path) {
			bool b_result = !string.IsNullOrEmpty(_file_path);
			if ( b_result == false) {
				this.m_error = "Image file path cannot be empty;";
				return b_result;
			}

			this.m_command = Utils.ConvertGraphicToSBPL(_file_path, false);
			b_result = IsValid();
			if ( b_result == false) {
				this.m_error = "SATO printer utility internal error occurred:\ncannot convert the file specified to command;";
				return b_result;
			}

			return b_result;
		}

		public bool IsValid() { return !string.IsNullOrEmpty(this.m_command);  }
	}
}