﻿using System.Windows;
using System.Windows.Media;
using System.Windows.Media.Imaging;

using Gma.QrCodeNet.Encoding;

namespace Sato.Core.Render {

	public class QrBitmap {

		private WriteableBitmap m_qrcode = null;
		private string m_error = "";
		private int    m_scale = 1 ;
		private Color  m_b_clr = Colors.Transparent;
		private Color  m_f_clr = Color.FromArgb(0xff, 0x1B, 0xA1, 0xE2);

		public QrBitmap(){ }

		public Color BackColor { get { return this.m_b_clr; } set { this.m_b_clr = value; } }
		public WriteableBitmap Bitmap { get { return this.m_qrcode; } }

		public string Error    { get { return this.m_error; } }
		public Color ForeColor { get { return this.m_f_clr; } set { this.m_f_clr = value; } }

		public bool Generate(BitMatrix _mtx) {
			bool b_result = (0 < _mtx.Height && 0 < _mtx.Width);
			if ( b_result == false) {
				 m_error  = "QrBitmap::Generate(): bit matrix provided is empty;";
				return b_result;
			}
			if (this.Scale == 0) {
				m_error   = "QrBitmap::Generate(): bitmap scale cannot be zero;";
			}

			int n_heigh = _mtx.Height * this.Scale;
			int n_width = _mtx.Width  * this.Scale;

			Sato.Core.Data.QrMatrix mtx_scaled = new Sato.Core.Data.QrMatrix();
			if (mtx_scaled.Create(ref _mtx, this.Scale) == false ) {
				m_error = mtx_scaled.Error;
				return b_result;
			}
			bool [,] mtx_data = mtx_scaled.Data;

			m_qrcode = new WriteableBitmap(
				n_width,
				n_heigh, 96, 96, PixelFormats.Bgra32, null);

			int px_offset = (m_qrcode.PixelWidth * m_qrcode.Format.BitsPerPixel) / 8; // stride;

			for (int x_ = 0; x_ < m_qrcode.PixelWidth; x_ ++) {
				for (int y_ = 0; y_ < m_qrcode.PixelHeight; y_ ++) {

					Int32Rect rc_ = new Int32Rect(x_, y_, 1, 1);
					if (mtx_data[x_, y_]) {
						byte[] clr_data = { m_f_clr.B, m_f_clr.G, m_f_clr.R, (byte)m_f_clr.A };
						m_qrcode.WritePixels(rc_, clr_data, px_offset, 0);
					} else {
						byte[] clr_data = { m_b_clr.B, m_b_clr.G, m_b_clr.R, (byte)m_b_clr.A };
						m_qrcode.WritePixels(rc_, clr_data, px_offset, 0);
					}
				}
			}
			return (b_result = true);
		}

		public int Scale {
			get { return this.m_scale; } set { this.m_scale = value; }
		}

	}
}
