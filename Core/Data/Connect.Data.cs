﻿using System;
using System.Collections.Generic;

namespace Sato.Core.Data
{
	public class Connect {
		public string Id     { get; private set; }
		public string Name   { get;         set; }
		public string Tcp    { get;         set; }
		public uint   Port   { get;         set; }
		public string Note   { get;         set; }
		public uint   Timeout{ get;         set; }
		public string Error  { get; private set; }

		public Connect () {
			Id      = Guid.NewGuid().ToString();
			Name    = "";
			Tcp     = "";
			Port    =  9100;  // default SATO printer tcp/ip port;
			Error   = "";
			Timeout =  2500;
		}

		public Connect(string _Id) {
			Id      = _Id;
			Name    = "";
			Tcp     = "";
			Port    = 9100;
			Error   = "";
			Timeout = 2500;
		}

		public Connect (string _name, string _tcp, uint _port = 9100, uint _timeout = 2500) {
			Id      = Guid.NewGuid().ToString();
			Name    = _name;
			Tcp     = _tcp ;
			Port    = _port==0 ? 9100 : _port;
			Error   = "";
			Timeout = _timeout == 0? 2500 : _timeout;
		}

		public bool Is () {
			if (Id   == null || Id   == "") { Error = "Connect identifier is not set;"; return false; }
			if (Name == null || Name == "") { Error = "Connect name is not set;"      ; return false; }
			if (Tcp  == null || Tcp  == "") { Error = "Connect TCP/IP is not set;"    ; return false; }
			return (true);
		}

		public bool Save () {
			bool b_result = Is();

			if (false == b_result)
				return   b_result;

			Sato.Core.Stg.Registry stg_ = new Sato.Core.Stg.Registry();
			stg_.Add(this.Id, "name"   , this.Name);
			stg_.Add(this.Id, "tcp"    , this.Tcp );
			stg_.Add(this.Id, "port"   , this.Port);
			stg_.Add(this.Id, "timeout", this.Timeout);
			if (this.Note != null)
			stg_.Add(this.Id, "note"   , this.Note);

			return b_result;
		}
	}

	public class Connect_Enum {
		private List<Connect> conn_lst = new List<Connect>();

		public  List<Connect> List() { return conn_lst; }

		public Connect_Enum() {			
		}

		public void Append(Connect _conn) {
			conn_lst.Add(_conn);
		}

		public Connect Get(int n_index) {

			if (null == conn_lst || 0 > n_index || n_index >= conn_lst.Count) {
				return new Connect();
			}
			else {
				return conn_lst[n_index];
			}
		}

		public bool Load () {

			Sato.Core.Stg.Registry reg_stg = new Sato.Core.Stg.Registry();

			bool   b_result = reg_stg.Load(ref conn_lst);
			return b_result;
		}

		public bool Remove (string _s_conn_id) {
			Sato.Core.Stg.Registry reg_stg = new Sato.Core.Stg.Registry();

			bool   b_result = reg_stg.Delete(_s_conn_id);
			return b_result;
		}

	}
}
