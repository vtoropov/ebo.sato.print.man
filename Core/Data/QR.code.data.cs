﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Gma.QrCodeNet.Encoding;

namespace Sato.Core.Data {

	public class QrParser {

		public string Error     { get; private set; }
		public BitMatrix Matrix { get; private set; }

		public QrParser() {
			Error = "#n/a";
		}

		public string Encoded (string _input) {

			if (string.IsNullOrEmpty(_input)) {
				this.Error = "QrParser: input string is null or empty;";
				return null;
			}

			QrEncoder encoder = new QrEncoder(ErrorCorrectionLevel.M);
			QrCode qrCode;
			if (encoder.TryEncode(_input, out qrCode) == false) {
				this.Error = "QrEncoder::TryEncode(): string is large;";
				return null;
			}

			this.Matrix = qrCode.Matrix;

			string encoded = "";

			for (int x_ = 0; x_ < this.Matrix.Width; x_++) {
				for (int y_ = 0; y_ < this.Matrix.Height; y_++) {
					if (this.Matrix[x_, y_]) { encoded += "1"; }
					else { encoded += "0";  }
				}
				encoded += "\n";
			}
			return encoded;
		}

		public bool Is () {
			return (this.Matrix.Height > 0 && this.Matrix.Width > 0);
		}

	}
}
