﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using SATOPrinterAPI;

namespace Sato.Core.Data {

	public class PrinterData {

		public string ControlCharConvert(string data) {

			Dictionary<char, string> dict_ = this.ControlCharList().ToDictionary(x => x.Value, x => x.Key);

			foreach  (char key_ in dict_.Keys) {
				data = data.Replace(key_.ToString(), dict_[key_]);
			}
			return data;
		}

		public Dictionary<string, char> ControlCharList() {

			Dictionary<string, char> dict_ = new Dictionary<string, char>();
			dict_.Add("[NUL]", '\u0000');
			dict_.Add("[SOH]", '\u0001');
			dict_.Add("[STX]", '\u0002');
			dict_.Add("[ETX]", '\u0003');
			dict_.Add("[EOT]", '\u0004');
			dict_.Add("[ENQ]", '\u0005');
			dict_.Add("[ACK]", '\u0006');
			dict_.Add("[BEL]", '\u0007');
			dict_.Add("[BS]" , '\u0008');
			dict_.Add("[HT]" , '\u0009');
			dict_.Add("[LF]" , '\u000A');
			dict_.Add("[VT]" , '\u000B');
			dict_.Add("[FF]" , '\u000C');
			dict_.Add("[CR]" , '\u000D');
			dict_.Add("[SO]" , '\u000E');
			dict_.Add("[SI]" , '\u000F');
			dict_.Add("[DLE]", '\u0010');
			dict_.Add("[DC1]", '\u0011');
			dict_.Add("[DC2]", '\u0012');
			dict_.Add("[DC3]", '\u0013');
			dict_.Add("[DC4]", '\u0014');
			dict_.Add("[NAK]", '\u0015');
			dict_.Add("[SYN]", '\u0016');
			dict_.Add("[ETB]", '\u0017');
			dict_.Add("[CAN]", '\u0018');
			dict_.Add("[EM]" , '\u0019');
			dict_.Add("[SUB]", '\u001A');
			dict_.Add("[ESC]", '\u001B');
			dict_.Add("[FS]" , '\u001C');
			dict_.Add("[GS]" , '\u001D');
			dict_.Add("[RS]" , '\u001E');
			dict_.Add("[US]" , '\u001F');
			dict_.Add("[DEL]", '\u007F');
			return dict_;
		}

		public string ControlCharReplace(string data) {

			Dictionary<string, char> dict_ = this.ControlCharList();
			foreach  (string key in  dict_.Keys) {
				data = data.Replace( key, dict_[key].ToString());
			}
			return data;
		}
	}

}
