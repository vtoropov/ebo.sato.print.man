﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Gma.QrCodeNet.Encoding;

namespace Sato.Core.Data {

	public class QrMatrix {
		private bool [,] m_mtx  = new bool[0,0];
		private string  m_error = "";

		public QrMatrix() { }

		public bool Create (ref BitMatrix _mtx, int _scale = 1){
			bool   b_result = false;
			if (1 > _mtx.Width || 1 > _mtx.Height) {
				m_error = "Input matrix is empty;";
				return b_result;
			}
			if (1 > _scale) {
				m_error = "Scale value cannot be less than 1 (one);";
				return b_result;
			}
			int n_row_off = 0; // row index offset; 
			int n_col_off = 0; // col index offset;

			m_mtx = new bool [_mtx.Width * _scale, _mtx.Height * _scale]; // [cols, rows];

			// https://stackoverflow.com/questions/2843987/array-size-length-in-c-sharp

			for (int i_ = 0; i_ < _mtx.Width; i_ ++)     {        // cols;
				for (int j_ = 0; j_ < _mtx.Height; j_++) {        // rows;
					for (int x_ = 0; x_ < _scale ; x_++) {        // TODO: copying byte range should be considered;
						for (int y_ = 0; y_ < _scale; y_++) {
							m_mtx[x_ + n_col_off, y_ + n_row_off] = _mtx[i_, j_];
						}
					}
					n_row_off += _scale;
				}
				n_row_off  = 0;
				n_col_off += _scale;
			}
			
			return (b_result = true);
		}

		public bool[,] Data { get { return m_mtx;  }  }

		public string Error {
			get { return m_error; }
		}
	}
}
/*
 original:x < original:GetWidth() {
	original:y < original:GetWidth() {
		original:v = original:mtx[x,y];
		for (int x < scale) {
			for (int y < scale) {
				target:v[
				x + offset:x,
				y + offset:y] = original:v;
			}
		}
	offset:y += scale;
	}
	offset:x += scale;
	offset:y  = 0;
}

  x          
y 00 10     00 10 20 30 40 50
  01 11     01 11 21 31 41 51
            02 12 22 32 42 52
            03 13 23 33 43 53
            04 14 24 34 44 54
            05 15 25 35 45 55
 */
