﻿/*
 *	Created by Tech_dog (ebontrop@gmail.com) on 17-Nov-2019 at 4:29:33p, UTC+7, Novosibirsk, Tulenina, Sunday;
 *	This is Ebo Pack shared library generic Win32 API error wrapper interface declaration file;
 *  -----------------------------------------------------------------------------
 *  This interface is based on:
 *  http://reflector.webtropy.com/default.aspx/4@0/4@0/DEVDIV_TFS/Dev10/Releases/RTMRel/wpf/src/Base/MS/Internal/interop/ErrorCodes@cs/1305600/ErrorCodes@cs
*/
using System;
using System.ComponentModel;
using System.Diagnostics;
using System.Globalization;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Security;
using System.Security.Permissions;

namespace shared.sys_core {

	 //  
	 // / Wrapper for common Win32 status codes. 
	 // / </summary>
	[StructLayout(LayoutKind.Explicit)]
	internal struct Win32Error {
		[FieldOffset(0)]
		private readonly  int m_value;

		public Win32Error(int _code) {
			m_value = _code;
		}
		
		public static readonly Win32Error ERROR_SUCCESS               = new Win32Error(   0); // The operation completed successfully; 
		public static readonly Win32Error ERROR_INVALID_FUNCTION      = new Win32Error(   1); // Incorrect function;
		public static readonly Win32Error ERROR_FILE_NOT_FOUND        = new Win32Error(   2); // The system cannot find the file specified; 
		public static readonly Win32Error ERROR_PATH_NOT_FOUND        = new Win32Error(   3); // The system cannot find the path specified;
		public static readonly Win32Error ERROR_TOO_MANY_OPEN_FILES   = new Win32Error(   4); // The system cannot open the file;
		public static readonly Win32Error ERROR_ACCESS_DENIED         = new Win32Error(   5); // Access is denied;
		public static readonly Win32Error ERROR_INVALID_HANDLE        = new Win32Error(   6); // The handle is invalid;
		public static readonly Win32Error ERROR_OUTOFMEMORY           = new Win32Error(  14); // Not enough storage is available to complete this operation;
		public static readonly Win32Error ERROR_NO_MORE_FILES         = new Win32Error(  18); // There are no more files; 
		public static readonly Win32Error ERROR_SHARING_VIOLATION     = new Win32Error(  32); // The process cannot access the file because it is being used by another process; 
		public static readonly Win32Error ERROR_INVALID_PARAMETER     = new Win32Error(  87); // The parameter is incorrect;
		public static readonly Win32Error ERROR_INSUFFICIENT_BUFFER   = new Win32Error( 122); // The data area passed to a system call is too small; 
		public static readonly Win32Error ERROR_NESTING_NOT_ALLOWED   = new Win32Error( 215); // Cannot nest calls to LoadModule; 
		public static readonly Win32Error ERROR_KEY_DELETED           = new Win32Error(1018); // Illegal operation attempted on a registry key that has been marked for deletion;
		public static readonly Win32Error ERROR_NO_MATCH              = new Win32Error(1169); // There was no match for the specified key in the index;
		public static readonly Win32Error ERROR_BAD_DEVICE            = new Win32Error(1200); // An invalid device was specified;
		public static readonly Win32Error ERROR_CANCELLED             = new Win32Error(1223); // The operation was canceled by the user;
		public static readonly Win32Error ERROR_INVALID_WINDOW_HANDLE = new Win32Error(1400); // Invalid window handle; 
		public static readonly Win32Error ERROR_TIMEOUT               = new Win32Error(1460); // This operation returned because the timeout period expired; 
		public static readonly Win32Error ERROR_INVALID_DATATYPE      = new Win32Error(1804); // The specified datatype is invalid;
	}

	class CError {
	}
}
