﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Win32;

using Sato.Core.Data;

namespace Sato.Core.Stg {

	public class Registry {
		private const string s_root_entry = @"Software";
		private const string s_sato_entry = @"Sato"    ; private string s_sato_path = s_root_entry + @"\" + s_sato_entry;
		private const string s_conn_entry = @"Connects"; private string s_conn_path = s_sato_entry + @"\" + s_conn_entry;

#if __do_not_use_it_because_it_requires_admin_permissions__
		Microsoft.Win32.RegistryKey root_ = Microsoft.Win32.Registry.LocalMachine.OpenSubKey(s_root_, true);
#else
		Microsoft.Win32.RegistryKey root_ = Microsoft.Win32.Registry.CurrentUser.OpenSubKey(s_root_entry, true);
#endif
		public Registry() {
			if (this.Is() == false) {
				root_.CreateSubKey(s_conn_path);
			}
		}

		public bool Add(string _id, string _name, string _value) {
			bool b_result = false;

			if (_name == "" || _value == "" || _id == "")
				return b_result;

			RegistryKey conn_enum = root_.OpenSubKey(s_conn_path, true);
			RegistryKey conn_key  = conn_enum.CreateSubKey(_id);
			conn_key.SetValue(_name, _value);

			return b_result;
		}

		public bool Add(string _id, string _name, uint _value)
		{
			bool b_result = false;

			if (_name == "" || _id == "")
				return b_result;

			RegistryKey conn_enum = root_.OpenSubKey(s_conn_path, true);
			RegistryKey conn_key  = conn_enum.CreateSubKey(_id);
			conn_key.SetValue(_name, _value);

			return b_result;
		}

		public bool Delete(string _entry)
		{
			bool b_result = false;

			if (_entry == "")
				return b_result;
			string s_path = s_conn_path + "\\" + _entry;

			root_.DeleteSubKey(s_path, true);
			return (b_result = true);
		}

		public bool Load(ref List<Connect> __list) {
			bool b_result = false;

			Microsoft.Win32.RegistryKey conn_enum = root_.OpenSubKey(s_conn_path);
			if (null == conn_enum || 0 == conn_enum.SubKeyCount)
				return b_result;

			string[] sub_keys = conn_enum.GetSubKeyNames();

			foreach (string entry_ in sub_keys) {

				RegistryKey entry_key = conn_enum.OpenSubKey(entry_);

				Connect conn_ = new Connect(entry_);

				string[] values = entry_key.GetValueNames();
				foreach (string value_ in values) {
					if (value_ == "name") { conn_.Name = (string)entry_key.GetValue(value_); }
					if (value_ == "tcp" ) { conn_.Tcp  = (string)entry_key.GetValue(value_); }
					if (value_ == "note") { conn_.Note = (string)entry_key.GetValue(value_); }
					if (value_ == "port") {
						string s_port = (string)entry_key.GetValue(value_);
						uint   u_port = 0;
						if (uint.TryParse(s_port, out u_port))
							conn_.Port =  u_port;
					}
					if (value_ == "timeout") {
						string s_time = (string)entry_key.GetValue(value_);
						uint   u_time = 0;
						if (uint.TryParse(s_time, out u_time))
							conn_.Timeout = u_time;
					}
				}
				__list.Add(conn_);
			}
			return b_result;
		}

		private bool Is() {
			return (root_.OpenSubKey(s_sato_entry) != null);
		}
	}
}
